extern crate image;

#[macro_use]
extern crate clap;

use std::path::Path;

use image::{GenericImageView, DynamicImage};
use ansi_term::Colour::{Green, White, Yellow};
use std::process::exit;

#[derive(Debug)]
enum Rotation {
    None,
    Degree(u16),
}

#[derive(Debug)]
enum BrightnessAlgorithm {
    Average,
    Lightness,
    Luminosity
}

// wir2.jpg exif: orientated to right -> Bild nach rechts gedreht
// lena-und-ich.jpg: kein exif -> korrekt angezeigt
// kissing.jpg exif orientation straigt (width ist kleiner als height) -> korrekt angezeigt
// wir.jpg exif rotated to right (width ist größer als height) -> Bild nach rechts gedreht
fn main() {

    let matches = clap_app!(myapp =>
        (@arg image: -i --image +takes_value +required "Path to the image file to convert to ascii.")
        (@arg color: -c --color +takes_value "Color of ascii characters printed to the terminal (white, green, or yellow). Defaults to white")
        (@arg brightness: -b --brightness +takes_value "Algorithm for brightness calculation (average, luminosity, lightness). Defaults to average")
        (@arg invert: -n --invert "Algorithm for brightness calculation (average, luminosity, lightness). Defaults to average")
    ).get_matches();

    let path_to_image = image_parameter(matches.value_of("image"));

    let print_color = color_parameter(matches.value_of("color"));

    let brightness_algorithm = brightness_parameter(matches.value_of("brightness"));

    let invert: bool = matches.is_present("invert");

    let rotation: Rotation = read_orientation(path_to_image);

    let mut image: DynamicImage = load_image(path_to_image);

    image = resize_image(image);

    if let Rotation::Degree(_degree) = rotation {
        image = rotate_image(image, _degree);
    }

    let ascii_image = convert_image_to_ascii(image, brightness_algorithm, invert);

    print_ascii_image(ascii_image, print_color);
}

 fn image_parameter(image_parameter: Option<&str>) -> &str {
    println!("image parameter {:?}", image_parameter);
    let image_argument = image_parameter.unwrap();
    let image_path = Path::new(image_argument);
    if !image_path.exists() {
        println!("Path to image is invalid");
        exit(1);
    }

    return image_argument.clone();
}

fn color_parameter(color_parameter: Option<&str>) -> ansi_term::Colour {

    // let color_name = color_parameter.unwrap();
    let mut color: ansi_term::Color = White;

    if let Some(color_name) = color_parameter {
        match color_name {
            "green" => color = Green,
            "white" => color = White,
            "yellow" => color = Yellow,
            _ => color = White,
        }

    }
    return color;
}

fn brightness_parameter(brightness_parameter: Option<&str>) -> BrightnessAlgorithm {

    let mut brightness = BrightnessAlgorithm::Average;

    if let Some(brightness_name) = brightness_parameter {
        match brightness_name {
            "average" => brightness = BrightnessAlgorithm::Average,
            "luminosity" => brightness = BrightnessAlgorithm::Luminosity,
            "lightness" => brightness = BrightnessAlgorithm::Lightness,
            _ => brightness = BrightnessAlgorithm::Average,
        }
    }

    return brightness;
}

// fn invert_parameter(invert_parameter: Option<&str>) -> bool {
//     println!("invert_parameter {:?}", invert_parameter);
//     return true;
// }

fn load_image(path_to_image: &str) -> DynamicImage {
    let image_path = Path::new(path_to_image);
    let image = image::open(image_path).unwrap();

    let (width, height) = image.dimensions();
    println!("Image successfully loaded {} x {}", width, height);

    return image;
}

fn read_orientation(path_to_image: &str) -> Rotation {
    let mut rotation: Rotation = Rotation::None;

    match rexif::parse_file(&path_to_image) {
        Ok(exif) => {
            for entry in &exif.entries {
                // entry.value
                if entry.tag != rexif::ExifTag::Orientation {
                    continue;
                }

                match entry.value {
                    rexif::TagValue::U16(ref v) => {
                        let n = v[0];
                        match n {
                            1 => rotation = Rotation::None,
                            3 => rotation = Rotation::Degree(180), //"Upside down",
                            6 => rotation = Rotation::Degree(90), // "Rotated to left",
                            8 => rotation = Rotation::Degree(270), // "Rotated to right",
                            9 => rotation = Rotation::None,
                            _ => rotation = Rotation::None,
                        }
                    }
                    _ => (),
                }
            }
        },
        Err(e) => {
            print!("Error in {}: {}", &path_to_image, e)
        }
    }

    return rotation;
}

fn rotate_image(original_image: DynamicImage, degree: u16) -> DynamicImage {
    let image: DynamicImage;
    if degree == 90 {
        image = original_image.rotate90();
    } else if degree == 180 {
        image = original_image.rotate180();
    } else if degree == 270 {
        image = original_image.rotate270();
    } else {
        image = original_image;
    }

    println!("Image rotated by {}", degree);

    return image;
}

fn convert_image_to_ascii(image: DynamicImage, brightness_algorithm: BrightnessAlgorithm, invert: bool) -> Vec<Vec<char>> {
    let (resized_width, resized_height) = image.dimensions();

    let mut ascii_image = Vec::new();
    for y in 0..resized_height {
        let mut row = Vec::new();

        for x in 0..resized_width {
            let pixel = image.get_pixel(x, y);
            let mut brightness = pixel_to_brightness(pixel.0, &brightness_algorithm);
            if invert {
                brightness = 255 - brightness;
            }
            let ascii_char = brightness_to_ascii(brightness);
            row.push(ascii_char);
        }

        ascii_image.push(row);
    }

    return ascii_image;
}


fn resize_image(image: DynamicImage) -> DynamicImage {
    let (width, height) = image.dimensions();

    let new_width: u32;
    let new_height: u32;
    if width > height {
        new_width = 200;
        new_height = (height * new_width) / width;
    } else {
        new_height = 200;
        new_width = (width * new_height) / height;
    }

    let rotated_image = image.resize_exact(new_width, new_height, image::imageops::Gaussian);

    let (width, height) = rotated_image.dimensions();
    println!("Image resized to {} x {}", width, height);

    return rotated_image;
}

fn pixel_to_brightness(pixel: [u8; 4], brightness_algorithm: &BrightnessAlgorithm) -> u32 {

    let brightness: f32;
    match brightness_algorithm {
        BrightnessAlgorithm::Average => brightness = brightness_by_average(pixel),
        BrightnessAlgorithm::Luminosity => brightness = brightness_by_luminosity(pixel),
        BrightnessAlgorithm::Lightness => brightness = brightness_by_lightness(pixel),
    }

    brightness.round() as u32
}

fn brightness_by_average(pixel: [u8; 4]) -> f32 {
    (pixel[0] as f32 + pixel[1] as f32 + pixel[2] as f32) / 3.0
}

fn brightness_by_luminosity(pixel: [u8; 4]) -> f32 {
    0.21 * pixel[0] as f32 + 0.72 * pixel[1] as f32 + 0.07 * pixel[2] as f32
}

fn brightness_by_lightness(pixel: [u8; 4]) -> f32 {
    let max = vec![pixel[0] as f32, pixel[1] as f32, pixel[2] as f32].iter().fold(0.0f32, |max, &val|
        if val > max { val } else { max }
    );

    let min = vec![pixel[0] as f32, pixel[1] as f32, pixel[2] as f32].iter().fold(0.0f32, |min, &val|
        if val < min { val } else { min }
    );

    max + min / 2.0
}

fn brightness_to_ascii(brightness: u32) -> char {
    let ascii_chars: String = String::from("`^\",:;Il!i~+_-?][}{1)(|\\/tfjrxnuvczXYUJCLQ0OZmwqpdbkhao*#MW&8%B@$");

    let mut char_index = ascii_chars.len() as u32 * brightness / 255;


    if char_index >= ascii_chars.len() as u32 {
        char_index = (ascii_chars.len() - 1) as u32;
    }

    ascii_chars.chars().nth((char_index) as usize).unwrap()
}

fn print_ascii_image(ascii_image: Vec<Vec<char>>, color: ansi_term::Color) {
    for row in ascii_image {
        for pixel in row {
            print!("{}{}{}", color.paint(pixel.to_string()) , color.paint(pixel.to_string()), color.paint(pixel.to_string()));
        }

        println!();
    }
}
